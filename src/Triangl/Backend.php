<?php

namespace Triangl;

use Symfony\Component\HttpFoundation\Request;

use Silex\ServiceProviderInterface;

use Triangl\Provider\BackendNavigationServiceProvider;
use Triangl\Provider\LanguageNavigationServiceProvider;
use Triangl\Provider\DomainNavigationServiceProvider;
use Triangl\Provider\EntityGridMenuServiceProvider;
use Triangl\Provider\FileManagerServiceProvider;
use Triangl\Provider\BackendApiServiceProvider;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;
use Triangl\Component\BuildGridEvent;
use Triangl\Component\BuildFormEvent;
use Triangl\Component\BuildBackendMenuEvent;
use Triangl\Component\EntityGridWidget;
use Triangl\Component\EntityFormWidget;
use Triangl\Component\EntityGridFilterWidget;
use Triangl\Component\EntityGridAssociationWidget;
/*
 * Triangl backend module.
 */
class Backend implements ServiceProviderInterface {
    const DEF_URL = "admin";
    
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {
        $this->buildLanguageMenu($app);
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {        
        // Default configurations.
        $app['backend.url'] = Backend::DEF_URL;
        
        // Grid configuration.
        $app['dispatcher']->addListener('backend.build.grid', function (BuildGridEvent $event) {
            if ( $event->getClassName() == '\Triangl\Entity\Security\User' ) {
                $event->setProperties( array('login', 'user_role_associations', 'user_domain_associations') );
            }
            else if ( $event->getClassName() == '\Triangl\Entity\Security\Domain' ) {
                $event->setProperties( array('name') );
            }
            else if ( $event->getClassName() == '\Triangl\Entity\Security\Role' ) {
                $event->setProperties( array('name', 'alias') );
            }
            else if ( $event->getClassName() == '\Triangl\Entity\Security\Firewall' ) {
                $event->setProperties( array('name', 'resource', 'action') );
            }
        });
        
        // Form configuration.
        $app['dispatcher']->addListener('backend.build.form', function (BuildFormEvent $event) {
            if ( $event->getClassName() == '\Triangl\Entity\Security\User' ) {
                $event->setProperties( array('login') );
            }
        });
        
        // Backend navigation.
        $app['dispatcher']->addListener('backend.build.navigation', function (BuildBackendMenuEvent $event) {
            $this->buildBackendMenu( $event->getBuilder(), $event->getContext() );
        });
        
        // Register template folder.
        $app['twig.loader.filesystem']->prependPath(__DIR__ . "/../../views");
        
        // Handle firewall setting for backend part.
        $app['triangl.security']->addFirewall( array(
            'login' => array(
                'pattern' => '^/' . $app['backend.url'] . '/login',
            )   
        ) );
        $app['triangl.security']->addFirewall( array(
            'admin' => array(
                'pattern' => '^/' . $app['backend.url'],
                'form' => array(
                    'login_path' => '/' . $app['backend.url'] . '/login', 
                    'check_path' => '/' . $app['backend.url'] . '/authenticate',
                    'username_parameter' => 'form[username]',
                    'password_parameter' => 'form[password]'
                ),
                'users' => $app->share( function() use ($app) {
                    return $app['db.orm.em']->getRepository('Triangl\Entity\Security\User');
                } ),
                'logout' => array('logout_path' => '/' . $app['backend.url'] . '/logout', 'target_url' => '/' . $app['backend.url'] . '/login')
            )
        ) );
        
        // Services.
        $app->register( new BackendNavigationServiceProvider() );        
        $app->register( new LanguageNavigationServiceProvider() );
        $app->register( new DomainNavigationServiceProvider() );
        $app->register( new EntityGridMenuServiceProvider() );
        $app->register( new FileManagerServiceProvider() );
        $app->register( new BackendApiServiceProvider() );
        $app['triangl.backend'] = $app->share( function ($app) {
            return new BackendHelper($app);
        } );
                
        // Widgets.
        $app['widget.entity.grid'] = $app->share(function() use ($app) {
            return new EntityGridWidget($app);
        });
        $app['widget.entity.grid.filter'] = $app->share(function() use ($app) {
            return new EntityGridFilterWidget($app);
        });
        $app['widget.entity.grid.association'] = $app->share(function() use ($app) {
            return new EntityGridAssociationWidget($app);
        }); 
        $app['widget.entity.form'] = $app->share(function() use ($app) {
            return new EntityFormWidget($app);
        });
                
        // Controllers.
        $app['backend.controller'] = $app->share(function() use ($app) {
            return new BackendController($app);
        });
        $app['backend.domain.controller'] = $app->share(function() use ($app) {
            return new BackendDomainController($app);
        });
        $app['backend.upload.file.controller'] = $app->share(function() use ($app) {
            return new BackendUploadFileController($app);
        });
        $app['backend.set_password.controller'] = $app->share(function() use ($app) {
            return new BackendSetPasswordController($app);
        });
        $app['backend.navigation_system.controller'] = $app->share(function() use ($app) {
            return new BackendNavigationSystemController($app);
        });
        $app['backend.entity_edit_association.controller'] = $app->share(function() use ($app) {
            return new EntityEditAssociationController($app);
        });

        // Routes.
        $app->get($app['backend.url'] . '/widget/grid/{className}/{page}', 'widget.entity.grid:indexAction')
            ->assert('page', '\d+')
            ->value('page', 0)
            ->bind('widget_grid');
        $app->get($app['backend.url'] . '/widget/grid/filter/{className}/{orderBy}/{filter}/{filterParam}/{filterValue}', 'widget.entity.grid.filter:indexAction')
            ->value('orderBy', 'id:ASC')
            ->value('filterValue', null)
            ->bind('widget_grid_filter');
        $app->get($app['backend.url'] . '/widget/grid/association/{className}/{parentClass}/{property}/{parentId}', 'widget.entity.grid.association:indexAction')
            ->value('value', null)
            ->bind('widget_grid_association');
        $app->match($app['backend.url'] . '/widget/form/{className}/{id}/{method}', 'widget.entity.form:indexAction')
            ->assert('id', '\d+')
            ->value('id', null)
            ->value('method', 'post')
            ->method('GET|POST|DELETE')
            ->bind('widget_form');
        $app->get($app['backend.url'], 'backend.controller:indexAction')
            ->bind('backend');
        $app->get($app['backend.url'] . '/login', 'backend.controller:loginAction')
            ->bind('backend_login');
        $app->get($app['backend.url'] . '/domain/{id}', 'backend.domain.controller:indexAction')
            ->assert('id', '\d+')
            ->bind('backend_domain');
        $app->post($app['backend.url'] . '/upload/{className}/{id}/{property}', 'backend.upload.file.controller:indexAction')
            ->bind('backend_upload');
        $app->match($app['backend.url'] . '/setPassword/{id}', 'backend.set_password.controller:indexAction')
            ->assert('id', '\d+')
            ->value('id', null)
            ->method('GET|POST')
            ->bind('backend_set_password');
        $app->get($app['backend.url'] . '/upload/preview/{fileName}', 'backend.upload.file.controller:previewAction')
            ->value('fileName', null)
            ->bind('backend_upload_preview');
        $app->get($app["backend.url"] . "/domains", "backend.navigation_system.controller:domainsAction")
            ->bind('backend_domains');
        $app->get($app["backend.url"] . "/roles", "backend.navigation_system.controller:rolesAction")
            ->bind('backend_roles');
        $app->get($app["backend.url"] . "/firewalls", "backend.navigation_system.controller:firewallsAction")
            ->bind('backend_firewalls');
        $app->get($app["backend.url"] . "/domainAssigment", "backend.navigation_system.controller:domainAssigmentAction")
            ->bind('backend_domain_assigment');
        $app->get($app["backend.url"] . "/roleAssigment", "backend.navigation_system.controller:roleAssigmentAction")
            ->bind('backend_role_assigment');
        $app->get($app["backend.url"] . "/firewallAssigment", "backend.navigation_system.controller:firewallAssigmentAction")
            ->bind('backend_firewall_assigment');
        $app->get($app["backend.url"] . "/users", "backend.navigation_system.controller:usersAction")
            ->bind('backend_users');
        $app->get($app["backend.url"] . "/languages", "backend.navigation_system.controller:languagesAction")
            ->bind('backend_languages');
        $app->get($app["backend.url"] . "/editAssociation/{className}/{filter}/{property}/{method}/{id}/{targetId}", "backend.entity_edit_association.controller:associationAction")
            ->value('id', null)
            ->value('targetId', null)
            ->bind('entity_edit_association');
        $app->get($app["backend.url"] . "/editAssociationClass/{className}/{associationClass}/{property}/{targetProperty}/{method}/{id}/{targetId}", "backend.entity_edit_association.controller:associationClassAction")
            ->value('id', null)
            ->value('targetId', null)
            ->bind('entity_edit_association_class');
        
        // Middleware.
        $app->before(function (Request $request, \Silex\Application $app) {
            // Enable domain filter and set it's parameter to currently selected domain.
            $selectedDomain = $app['backend.selector.domain']->getSelectedDomain();
            if ($selectedDomain) {
                $filter = $app['db.orm.em']->getFilters()->enable('domain');
                $filter->setParameter( 'domain_id', $selectedDomain->getId() );
            }
        });
    }
    
    /**
     * Builds backend menu.
     * @param \Triangl\Component\Navigation\MenuBuilder $builder
     * @param \Silex\Application $app
     */
    private function buildBackendMenu(MenuBuilder $builder, \Silex\Application $app) {
        $user = $app["security.user.instance"];
        $action = $app['triangl.security']->getActionByAlias('view');
        
        $menu = new MenuItemComposite("System", null, "off");
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\User', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Users", 'backend_users'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\Role', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Roles", 'backend_roles'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\User', $action)
             && $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\Role', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Role Assigment", 'backend_role_assigment'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\Firewall', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Firewalls", 'backend_firewalls'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\Role', $action) 
             && $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\Firewall', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Firewall Assigment", 'backend_firewall_assigment'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\Domain', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Domains", 'backend_domains'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\User', $action)
             && $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\Security\Domain', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Domain Assigment", 'backend_domain_assigment'
            ) );
        }        
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\ContentManagementSystem\Language', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Languages", 'backend_languages'
            ) );
        }
        
        $builder->pushChild($menu, true);
    }
        
    /**
     * Builds menu for languages.
     */
    private function buildLanguageMenu(\Silex\Application $app) {
        // TO - DO move to provider
        
        $em = $app['db.orm.em'];
        
        $menu = new MenuItemComposite("Languages");
        $first = true;
        foreach( $em->getRepository( '\Triangl\Entity\ContentManagementSystem\Language')->findAll() as $language ) {
            $item = new MenuItemLeaf(
                $language->getName(), "backend"
            );
            if ($first) {
                $item->setActive();
                $first = false;
            }
            $menu->pushChild($item);
        }
        
        $app['navigation.language']->pushChild($menu);
    }
}
