<?php

namespace Triangl;

use Symfony\Component\Validator\Constraints as Assert;

/*
 * Triangl backend controller.
 */
class BackendController extends Controller {
    /**
     * Index action.
     */
    public function indexAction() {
        // Init assets.        
        $this->app['triangl.bootstrap']->loadBootstrap();
        $this->app['triangl.backend']->loadFileUpload();
        $this->app["assets"]->addStyleSheet( array(
            "template" => "backend.css.twig"
        ) );
        $this->app["assets"]->addJavaScript( array(
            "template" => 'base.js.twig'
        ) );
        $this->app["assets"]->addJavaScript( array(
            "template" => 'backend.js.twig'
        ) );
        
        return $this->app->render( 'backend.html.twig' );
    }
    
    /**
     * Login action.
     */
    public function loginAction() {
        // Init assets.
        $this->app['triangl.bootstrap']->loadBootstrap();
        $this->app['triangl.backend']->loadFileUpload();
        $this->app["assets"]->addStyleSheet( array(
            "template" => "login.css.twig"
        ) );
        
        // Defaul data.
        $data = array(
            'username' => $this->app['session']->get('_security.last_username')
        );

        // Build form.
        $form = $this->app->form($data)
            ->add('username', 'text', array(
                'attr' => array('autofocus' => ''),
                'constraints' => array( new Assert\NotBlank() )                
            ) )
            ->add('password', 'password', array(
                'constraints' => array( new Assert\NotBlank() )
            ) )
            ->getForm();

        // Display result.
        return $this->app->render( 'login.html.twig', array(
            'login' => $form->createView(),
            'error' => $this->app['security.last_error']($this->app["request"])
        ) );
    }
}
