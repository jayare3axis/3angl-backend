<?php

namespace Triangl;

/*
 * Controller for backend api.
 */
class BackendApiController extends ApiController {
    /**
     *  Changes order between two items.
     *  @param string $className
     *  @param int $id
     *  @param int $targetId
     *  @param string $method
     */
    public function orderAction($className, $id, $targetId, $method) {
        $result = array();
        
        try {
            $em = $this->app['db.orm.em'];
            $instance = $em->getRepository($className)->find($id);
            $targetInstance = $em->getRepository($className)->find($targetId);
            
            if (!$instance || !$targetInstance) {
                throw new \InvalidArgumentException("Record was not found.");
            }
            
            // Swap orders.
            $ord = $instance->getOrd();
            $targetOrd = $targetInstance->getOrd();
            $instance->setOrd($targetOrd);
            $targetInstance->setOrd($ord);
            
            $em->persist($instance);
            $em->persist($targetInstance);            
            $em->flush();
            
            $result["success"] = true;
        }
        catch (\Exception $e) {
            $result = $this->formatException($e);
            $this->app->log($e);
        }
        
        return $this->app->json($result);
    }
}
