<?php

namespace Triangl;

/*
 * Used to select domain in backend.
 */
class BackendDomainController extends Controller {
    /**
     * Index action.
     * @param int $id
     */
    public function indexAction($id) {
        // Select domain by given id.
        $domain = $this->app['db.orm.em']->getRepository('\Triangl\Entity\Security\Domain')->find($id);
        if ($domain) {
            $this->app['backend.selector.domain']->selectDomain($domain);
        }
        
        return $this->app->redirect( $this->app->url('backend') );
    }
}
