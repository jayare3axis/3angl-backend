<?php

namespace Triangl;

use FileUpload;

use Triangl\Entity\FileField;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Handles uploading of files for backend part.
 */
class BackendUploadFileController extends Controller {
    /**
     * Handles file upload.
     * @param string $className
     * @param id $id
     * @param string $property
     */
    public function indexAction($className, $id, $property) {
        // TO - DO handle validator
        //$validator = new FileUpload\Validator\Simple(1024 * 1024 * 1024, ['image/png', 'image/jpg', 'video/x-flv', 'video/mp4', 'video/quicktime', 'video/x-msvideo', 'video/x-ms-wmv']);
        $validator = new FileUpload\Validator\Simple(2 * 1024 * 1024, ['image/png', 'image/jpg', 'image/jpeg']);
        $pathresolver = new FileUpload\PathResolver\Simple( $this->app['backend.upload.fm']->getUploadDir() );
        $filesystem = new FileUpload\FileSystem\Simple();
        
        $fileupload = new FileUpload\FileUpload($_FILES['files'], $_SERVER);
        $fileupload->setPathResolver($pathresolver);
        $fileupload->setFileSystem($filesystem);
        $fileupload->addValidator($validator);

        list($files, $headers) = $fileupload->processAll();

        foreach($headers as $header => $value) {
            header($header . ': ' . $value);
        }
        
        // Write field to entity
        $file = $files[0];
        $instance = $this->app['db.orm.em']->getRepository($className)->find($id);
        $metaData = $this->app['db.orm.em']->getClassMetadata($className);
        $value = new FileField($file->name, $file->size, $file->type);
        $metaData->setFieldValue($instance, $property, $value);
        $this->app['db.orm.em']->persist($instance);
        $this->app['db.orm.em']->flush();
        
        $result = array('files' => $files);
        return $this->app->json($result);
    }
    
    /**
     * Gets file preview.
     * @param string $fileName
     */
    public function previewAction($fileName) {
        $file = null;
        if ($fileName != null) {
            $path = $this->app['backend.upload.fm']->getUploadDir() . "/" . $fileName;
            $file = new File($path);
        }
        
        return $this->app->render( 'file_preview.html.twig', array('file' => $file) );
    }
}
