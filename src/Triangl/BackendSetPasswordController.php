<?php

namespace Triangl;

use Symfony\Component\Validator\Constraints as Assert;

use Triangl\Component\Alert\AlertBuilder;
use Triangl\Component\Alert\Alert;

/*
 * Triangl backend set user password controller.
 */
class BackendSetPasswordController extends Controller {
    /**
     * Index action.
     */
    public function indexAction($id) {
        $alerts = new AlertBuilder();
        $request = $this->app['request'];
        $em = $this->app["db.orm.em"];
        
        // Build form.
        $form = $this->app->form()
            ->add('password', 'password', array(
                'attr' => array('autofocus' => ''),
                'constraints' => array( new Assert\NotBlank() )                
            ) )
            ->add('password-again', 'password', array(
                'constraints' => array( new Assert\NotBlank() )
            ) )
            ->getForm();
        
        // Process request.
        if ($request->getMethod() == 'POST') {
            try {
                $form->handleRequest($request);                
                if ( $form->isValid() ) {
                   $data = $form->getData();
                   
                   if ($data['password'] != $data['password-again']) {
                       $alerts->pushAlert( new Alert("Passwords didn't match.", 'danger') );
                   }
                   else {
                       // Get encoded password.
                       $encoder = $this->app['security.encoder_factory']->getEncoder( $this->app->user() );
                       $password = $encoder->encodePassword($data['password'], '');
                       
                       // Modify instance.
                       $instance = $em->getRepository('Triangl\Entity\Security\User')->find($id);
                       $instance->setPassword($password);
                       $em->persist($instance);
                       $em->flush();
                                              
                       $alerts->pushAlert( new Alert("Password has been changed successfully.", 'success') );
                   }
                }
            } 
            catch (\Exception $e) {
                $this->app->log($e);
                $alerts->pushAlert( new Alert('There was error while processing your request.', 'danger') );
            }
        }
        
        // Display result.
        return $this->app->render( 'new_password.html.twig', array(
            'new_password' => $form->createView(),
            'action' => $this->app->url( 'backend_set_password', array("id" => $id) ),
            'alerts' => $alerts
        ) );
    }
}
