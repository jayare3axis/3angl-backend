<?php

namespace Triangl\Component;

use Triangl\Controller;

/*
 * Triangl entity association grid widget controller.
 */
class EntityGridAssociationWidget extends Controller {
    /**
     * Renders grid widget for given entity.
     * @param string $className class name
     * @param string $parentClass parent class name
     * @param string $property
     * @param string $parentId parent id
     */
    public function indexAction($className, $parentClass, $property, $parentId) {
        // Handle which properties to display.
        $event = new BuildGridEvent($className);
        $this->app['dispatcher']->dispatch('backend.build.grid', $event);
        $properties = $event->getProperties();
        
        $em = $this->app['db.orm.em'];
        $metaData = $em->getClassMetadata($parentClass);
                
        // Find relevant association mapping.
        $mapping = null;
        $mapping2 = null;
        foreach ($metaData->getAssociationMappings() as $value) {
            if ($value["mappedBy"] == $property) {
                $mapping = $value;
                
                // Find relevant association mapping.
                $associationClassMetaData = $em->getClassMetadata($mapping["targetEntity"]);
                foreach ($associationClassMetaData->getAssociationMappings() as $value) {
                    if ($value["targetEntity"] == $className || "\\" . $value["targetEntity"] == $className) {
                        $mapping2 = $value;
                    }
                }
                if ($mapping2 != null) {
                    break;
                }
            }
        }
        
        if ($mapping == null) {
            throw new \InvalidArgumentException("Association mapping mapped by property $property not found.");
        }
        if ($mapping2 == null) {
            throw new \InvalidArgumentException("Association mapping mapped to entity $className not found.");
        }
        
        // Find relevant instance.
        $instance = $em->getRepository($parentClass)->find($parentId);
        $collection = $metaData->getFieldValue( $instance, $mapping["fieldName"] );        
               
        $rows = array();
        $ids = array();
        foreach ($collection as $item) {             
            $i = $associationClassMetaData->getFieldValue($item, $mapping2["fieldName"]);              
            $i->getId(); // Just to load if is proxy.            
            array_push($rows, $i);
            
            $ids[] = $item->getId();
        }
        
        $grid = $this->app['db.orm.grid']->createGrid($className, -1, $properties, $rows);        
        $grid->addData( 'url', $this->app->url( 'widget_grid_association', array(
            'className' => $className,
            'parentClass' => $parentClass,
            'property' => $property,
            'parentId' => $parentId
        ) ) );
        
        // Fix id's to point to association class.
        $index = 0;
        foreach ($grid->getRows() as $row) {
            if ( !$row->isHeader() ) {
                $row->addData('id', $ids[$index++]);
            }
        }
        
        return $this->app['twig']->render(
            'grid_default_layout.html.twig', array(
                'grid' => $grid
            )
        );
    }
}
