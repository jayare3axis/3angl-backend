<?php

namespace Triangl\Component;

use Triangl\Controller;
use Triangl\Component\Alert\AlertBuilder;
use Triangl\Component\Alert\Alert;

/*
 * Triangl entity form widget controller.
 */
class EntityFormWidget extends Controller {
    /**
     * Renders form widget for given entity.
     * @param string $className class name
     * @param int $id instance id (optional)
     * @param string $method form method
     */
    public function indexAction($className, $id, $method) {        
        $alerts = new AlertBuilder();
        $request = $this->app['request'];
        
        // Handle which properties to display.
        $event = new BuildFormEvent($className);
        $this->app['dispatcher']->dispatch('backend.build.form', $event);
        $properties = $event->getProperties();
        
        // Build form for given entity.
        $form = $this->app['db.orm.form']->createForm($className, $id, $properties);
        
        // Process request.
        if ($request->getMethod() != 'GET') {
            try {
                $form->handleRequest($request);                
                if ( $request->getMethod() == 'DELETE' || $form->isValid() ) {
                    $data = $this->onParseData( $className, $form->getData() );
                    $id = $this->processData( $className, $id, $data, $request->getMethod(), $alerts );                    
                }
                else {
                    // TO - DO process validator messages
                    $alerts->pushAlert( new Alert('Form contains invalid value(s).', 'danger') ); 
                }
                if ( $request->getMethod() == 'POST' && $id == null) {
                    // When record is added set form to add new record instead
                    // of edit existing.
                    $form = $this->app['db.orm.form']->createForm($className, null);
                }
                if ( $request->getMethod() == 'DELETE') {
                    // When record is deleted set form to add new record instead
                    // of edit existing.
                    $form = $this->app['db.orm.form']->createForm($className, null);
                    $method = 'post';
                }
            } 
            catch (\Exception $e) {
                $this->app->log($e);
                $alerts->pushAlert( new Alert('There was error while processing your request.', 'danger') );
            }
        }
        
        // Handle form title.
        $title = $className;
        
        // Handle acton.
        $action = $this->app->url( 'widget_form', array(
            'className' => $className,
            'id' => $id,
            'method' => $method
        ) );
        
        // Add tooltip.
        if ($method == 'post') {
            if ($id == null) {
                $alerts->pushAlert( new Alert('Fill-out form to add new record.', 'info', 'plus') );
            }
            else {
                $alerts->pushAlert( new Alert('Fill-out form to update existing record.', 'info', 'wrench') );
            }
        }
        
        return $this->app['twig']->render(
            'widget_entity_form.html.twig', array(
                'className' => $className,
                'form' => $form->createView(),
                'title' => $title,
                'action' => $action,
                'method' => $method,
                'id' => $id,
                'alerts' => $alerts
            )
        );
    }
    
    /**
     * Called to parse data sent by client to real values.
     * @param string $className
     * @param array $data
     */
    protected function onParseData($className, array $data) {
        $result = array();
        foreach ($data as $key => $val)         {
            $result[$key] = $this->onParseDataItem($className, $key, $val);
        }
        return $result;
    }
    
    /**
     * Called to parse data item.
     * @param string $className
     * @param string $key
     * @param string $val
     * @return mixed
     */
    protected function onParseDataItem($className, $key, $val) {
        $result = $val;        
        $metaData = $this->app['db.orm.em']->getClassMetaData($className);
        
        // If target property is asociation, convert primary key to real instance.
        if ( $metaData->hasAssociation($key) ) {
            if ($val == null) {
                $result = null;
            }
            else {
                $mapping = $metaData->getAssociationMapping($key);
                $targetClassName = $mapping['targetEntity'];
                $targetRepository = $this->app['db.orm.em']->getRepository($targetClassName);
                $result = $targetRepository->find($val);
            }
        }
        
        return $result;
    }
    
    /**
     * Processes form data.
     * @param string $className
     * @param int $id
     * @param array $data
     * @param string $method
     * @param Triangl\Component\Alert\AlertBuilder $alerts     
     */
    protected function processData($className, $id, array $data, $method, AlertBuilder &$alerts) {
        if ($id == null && $method== 'POST') {
            return $this->onProcessAdd($className, $data, $alerts);
        }
        else if ($method== 'POST') {
            return $this->onProcessUpdate($className, $id, $data, $alerts);
        }
        else if ($method == 'DELETE') {
            return $this->onProcessDelete($className, $id, $alerts);            
        }
        return null;
    }
    
    /**
     * Process add request.
     * @param string $className
     * @param array $data
     * @param Triangl\Component\Alert\AlertBuilder $alerts
     */
    protected function onProcessAdd($className, array $data, AlertBuilder &$alerts) {
        $id = $this->app['db.orm.em']->getRepository($className)->create($data);
        $alerts->pushAlert( new Alert('Record was successfully added.', 'success') );
        return null;
    }
    
    /**
     * Process add request.
     * @param string $className
     * @param int id
     * @param array $data
     * @param Triangl\Component\Alert\AlertBuilder $alerts
     * @return int id
     */
    protected function onProcessUpdate($className, $id, array $data, AlertBuilder &$alerts) {
        $this->app['db.orm.em']->getRepository($className)->update($id, $data);
        $alerts->pushAlert( new Alert('Record was successfully updated.', 'success') );
        return $id;
    }
    
    /**
     * Process delete request.
     * @param string $className
     * @param int id
     * @param Triangl\Component\Alert\AlertBuilder $alerts
     * @return int id
     */
    protected function onProcessDelete($className, $id, AlertBuilder &$alerts) {
        $this->app['db.orm.em']->getRepository($className)->delete($id);
        $alerts->pushAlert( new Alert('Record was successfully deleted.', 'success') );
        return null;
    }
}
