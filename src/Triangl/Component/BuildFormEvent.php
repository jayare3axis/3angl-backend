<?php

namespace Triangl\Component;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event invoked before form for given entity is built.
 * Use it to override properties shown in the grid.
 */
class BuildFormEvent extends Event {
    private $className;
    private $properties;
    
    /**
     * Default constructor.
     */
    public function __construct($className) {
        $this->className = $className;
        $this->properties = null;
    }
    
    /**
     * Gets class name.
     * @return string
     */
    public function getClassName() {
        return $this->className;
    }
    
    /**
     * Sets properties for a grid.
     * @param array $properties
     * @return \Triangl\Component\BuildFormEvent this
     */
    public function setProperties(array $properties) {
        $this->properties = $properties;
        return $this;
    }
    
    /**
     * Gets properties.
     * @return array
     */
    public function getProperties() {
        return $this->properties;
    }
}
