<?php

namespace Triangl\Component;

use Triangl\Controller;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;
use Triangl\Component\BuildGridEvent;

/*
 * Triangl entity grid widget controller.
 */
class EntityGridWidget extends Controller {
    /**
     * Renders grid widget for given entity.
     * @param string $className class name
     * @param int $page specified page (optional)
     */
    public function indexAction($className, $page) {
        // Handle which properties to display.
        $event = new BuildGridEvent($className);
        $this->app['dispatcher']->dispatch('backend.build.grid', $event);
        $properties = $event->getProperties();
        
        $grid = $this->app['db.orm.grid']->createGrid($className, $page, $properties);
        $grid->addData( 'url', $this->app->url( 'widget_grid', array(
            "className" => $className,
            "page" => $page
        ) ) );
        
        if ( $grid->getPager() != null ) {
            $pagerMenu = new MenuItemComposite();
            // Handle <<
            if ($page > 0) {
                $item = new MenuItemLeaf("<<", "widget_grid");
                $item->pushArg("className", $className)
                    ->pushArg("page", $page - 1);
                $pagerMenu->pushChild($item);
            }
            
            for ($i = 0; $i < $grid->getPager()->getCount(); $i++) {
                $item = new MenuItemLeaf($i + 1, "widget_grid");
                $item->pushArg("className", $className)
                    ->pushArg("page", $i);
                if ( $i == $grid->getPager()->getIndex() ) {
                    $item->setActive();
                }
                $pagerMenu->pushChild($item);
            }
            
            // Handle >>
            if ($page < $grid->getPager()->getCount() - 1) {
                $item = new MenuItemLeaf(">>", "widget_grid");
                $item->pushArg("className", $className)
                    ->pushArg("page", $page + 1);
                $pagerMenu->pushChild($item);
            }
            $grid->getPager()->setMenu($pagerMenu);
        }
        
        return $this->app['twig']->render(
            'grid_default_layout.html.twig', array(
                'grid' => $grid
            )
        );
    }
}
