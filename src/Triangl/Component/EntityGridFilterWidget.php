<?php

namespace Triangl\Component;

use Triangl\Controller;

/*
 * Triangl entity filtered grid widget controller.
 */
class EntityGridFilterWidget extends Controller {
    /**
     * Renders grid widget for given entity.
     * @param string $className class name
     * @param string $orderBy order by field
     * @param string $filter filter name
     * @param string $filterParam filter param
     * @param string $filterValue filter value     
     */
    public function indexAction($className, $orderBy, $filter, $filterParam, $filterValue) {
        // Handle which properties to display.
        $event = new BuildGridEvent($className);
        $this->app['dispatcher']->dispatch('backend.build.grid', $event);
        $properties = $event->getProperties();
        
        // Aply filter.
        $f = $this->app['db.orm.em']->getFilters()->enable($filter);
        $f->setParameter($filterParam, $filterValue);
        
        // Handle order param
        $help = explode(':', $orderBy);
        $order = array($help[0] => $help[1]);
        
        $grid = $this->app['db.orm.grid']->createGrid($className, -1, $properties, null, array(), $order);
        $grid->addData( 'url', $this->app->url( 'widget_grid_filter', array(
            'className' => $className,
            'orderBy' => $orderBy,
            'filter' => $filter,
            'filterParam' => $filterParam,
            'filterValue' => $filterValue
        ) ) );
        
        return $this->app['twig']->render(
            'grid_default_layout.html.twig', array(
                'grid' => $grid
            )
        );
    }
}
