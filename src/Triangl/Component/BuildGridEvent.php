<?php

namespace Triangl\Component;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event invoked before grid for given entity is built.
 * Use it to override properties shown in the grid.
 */
class BuildGridEvent extends Event {
    private $className;
    private $properties;
    
    /**
     * Default constructor.
     */
    public function __construct($className) {
        $this->className = $className;
        $this->properties = null;
    }
    
    /**
     * Gets class name.
     * @return string
     */
    public function getClassName() {
        return $this->className;
    }
    
    /**
     * Sets properties for a grid.
     * @param array $properties
     * @return \Triangl\Component\BuildGridEvent this
     */
    public function setProperties(array $properties) {
        $this->properties = $properties;
        return $this;
    }
    
    /**
     * Gets properties.
     * @return array
     */
    public function getProperties() {
        return $this->properties;
    }
}
