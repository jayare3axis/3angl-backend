<?php

namespace Triangl\Component;

use Triangl\Application;

/*
 * File manager.
 */
class FileManager {
    protected $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Gets upload dir.
     * @return string
     */
    public function getUploadDir() {
        return $this->app['backend.upload.dir'];
    }
    
    /**
     * Gets upload path.
     * @return string
     */
    public function getUploadPath() {
        return $this->app['backend.upload.path'];
    }
}
