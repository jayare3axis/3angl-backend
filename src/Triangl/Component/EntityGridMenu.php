<?php

namespace Triangl\Component;

use Triangl\Application;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;

/**
 * Builds menu for entity grid component.
 */
class EntityGridMenu {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Creates grid menu for given entity.
     * @param string $className
     * @return Triangl\Component\Navigation\MenuBuilder
     */
    public function createMenu($className) {
        $menus = new MenuBuilder();
        
        $menu = new MenuItemComposite("Edit", null);
        $item = new MenuItemLeaf(
            'Add', 'widget_form', 'plus'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-add');
        $item->pushArg("className", $className);
        $menu->pushChild($item);

        /*$item = new MenuItemLeaf(
            'Copy', null, 'plus'
        );
        $item->setTag('info');
        $item->setClass('tool-copy');
        $menu->pushChild($item);*/

        $menus->pushChild($menu);
        return $menus;
    }
}
