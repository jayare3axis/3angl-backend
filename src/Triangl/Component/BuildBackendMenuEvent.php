<?php

namespace Triangl\Component;

use Symfony\Component\EventDispatcher\Event;

use Triangl\Component\Navigation\MenuBuilder;

/**
 * Event invoked when backend menu is being built.
 * Use it to modify backend menu based on modules.
 */
class BuildBackendMenuEvent extends Event {
    private $builder;
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(MenuBuilder $builder, \Silex\Application $app) {
        $this->builder = $builder;
        $this->app = $app;
    }
    
    /**
     * Gets the menu builder.
     * @return \Triangl\Component\Navigation\MenuBuilder
     */
    public function getBuilder() {
        return $this->builder;
    }
    
    /**
     * Gets the application container.
     * @return Triangl\Component\Navigation\Application
     */
    public function getContext() {
        return $this->app;
    }
}
