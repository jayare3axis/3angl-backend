<?php

namespace Triangl;

/**
 * Helper routines for backend module.
 */
class BackendHelper {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Loads file upload library from assets folder.
     */
    public function loadFileUpload() {
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['jq.file.upload.path'] . '/js/vendor/jquery.ui.widget.js'
        ) );
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['jq.file.upload.path'] . '/js/jquery.iframe-transport.js'
        ) );
        $this->app['assets']->addJavaScript( array(
            'path' => $this->app['jq.file.upload.path'] . '/js/jquery.fileupload.js'
        ) );
        $this->app['assets']->addStyleSheet( array(
            'path' => $this->app['jq.file.upload.path'] . '/css/jquery.fileupload.css'
        ) );
    }
}
