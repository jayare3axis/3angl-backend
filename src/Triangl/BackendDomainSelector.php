<?php

namespace Triangl;

use Triangl\Entity\Security\Domain;

/*
 * Handles selecting active domain for backend part.
 */
class BackendDomainSelector {
    private $app;
    private $repository;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
        $this->repository = $this->app['db.orm.em']->getRepository('\Triangl\Entity\Security\Domain');
    }
    
    /**
     * Sets target domain to be selected in backend.
     * @param \Triangl\Entity\ContentManagementSystem\Domain
     */
    public function selectDomain(Domain $domain) {
        $this->app['session']->set( 'backend.domain', $domain->getId() );
    }
    
    /**
     * Gets active domain.
     * @return \Triangl\ContentManagementSystem\Domain
     */
    public function getSelectedDomain() {
        $id = $this->app['session']->get('backend.domain');
        if ($id == null) {
            return $this->getDefaultDomain();
        }
        return $this->repository->find($id);
    }
    
    /**
     * Which domain is selected by default.
     * @return \Triangl\ContentManagementSystem\Domain
     */
    private function getDefaultDomain() {
        if ( $this->app['security.user.instance'] !== null ) {
            $collection = $this->app['security.user.instance']->getUserDomainAssociations();
            foreach ($collection as $association) {
                return $association->getDomain();
            }
        }
        return null;
    }
}
