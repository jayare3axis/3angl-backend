<?php

namespace Triangl;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;

/*
 * Controller for navigating between System menu.
 */
class BackendNavigationSystemController extends Controller {
    /**
     *  Users action.
     */
    public function usersAction() {
        $className = '\Triangl\Entity\Security\User';
        
        $userMenu = $this->app['db.orm.grid.menu']->createMenu($className);
        $menu = new MenuItemComposite("User", null);
        $item = new MenuItemLeaf(
            'Password', 'backend_set_password'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-password');
        $menu->pushChild($item);
        $userMenu->pushChild($menu);        
        
        $this->app['navigation.backend']->selectByRoute('backend_users');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $userMenu
        ) );
    }
    
    /**
     *  Roles action.
     */
    public function rolesAction() {
        $className = '\Triangl\Entity\Security\Role';
        
        $this->app['navigation.backend']->selectByRoute('backend_roles');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $this->app['db.orm.grid.menu']->createMenu($className)
        ) );
    }
    
    /**
     *  Firewalls action.
     */
    public function firewallsAction() {
        $className = '\Triangl\Entity\Security\Firewall';
        
        $this->app['navigation.backend']->selectByRoute('backend_firewalls');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $this->app['db.orm.grid.menu']->createMenu($className)
        ) );
    }
    
    /**
     *  Domains action.
     */
    public function domainsAction() {
        $className = '\Triangl\Entity\Security\Domain';
        
        $this->app['navigation.backend']->selectByRoute('backend_domains');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $this->app['db.orm.grid.menu']->createMenu($className)
        ) );
    }
    
    /**
     * Domain assigment action.
     */
    public function domainAssigmentAction() {
        $className1 = '\Triangl\Entity\Security\User';
        $className2 = '\Triangl\Entity\Security\Domain';
        $associationClass = '\Triangl\Entity\Security\UserDomainAssociation';
        
        // Get user id.
        $user = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $users = $repository->findAll();
        if ( count($users) > 0 ) {
            $user = $users[0];
        }
        
        // Menu for adding items.
        $addMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Add', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-add');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'user');
        $item->pushArg('targetProperty', 'domain');
        $item->pushArg('method', 'add');
        $menu->pushChild($item);
        $addMenu->pushChild($menu);
        
        // Menu for removing items.
        $remMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Remove', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-remove');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'user');
        $item->pushArg('targetProperty', 'domain');
        $item->pushArg('method', 'remove');        
        $menu->pushChild($item);
        $remMenu->pushChild($menu);
        
        $this->app['navigation.backend']->selectByRoute('backend_domain_assigment');
        return $this->app['twig']->render( 'backend_content_edit_association.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'associationClass' => $associationClass,
            'grid_menu1' => $addMenu,
            'grid_menu2' => $remMenu,
            'property' => 'user',
            'parentId' => ($user != null) ? $user->getId() : null
        ) );
    }
    
    /**
     * Role assigment action.
     */
    public function roleAssigmentAction() {
        $className1 = '\Triangl\Entity\Security\User';
        $className2 = '\Triangl\Entity\Security\Role';
        $associationClass = '\Triangl\Entity\Security\UserRoleAssociation';
        
        // Get user id.
        $user = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $users = $repository->findAll();
        if ( count($users) > 0 ) {
            $user = $users[0];
        }
        
        // Menu for adding items.
        $addMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Add', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-add');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'user');
        $item->pushArg('targetProperty', 'role');
        $item->pushArg('method', 'add');
        $menu->pushChild($item);
        $addMenu->pushChild($menu);
        
        // Menu for removing items.
        $remMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Remove', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-remove');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'user');
        $item->pushArg('targetProperty', 'role');
        $item->pushArg('method', 'remove');        
        $menu->pushChild($item);
        $remMenu->pushChild($menu);
        
        $this->app['navigation.backend']->selectByRoute('backend_role_assigment');
        return $this->app['twig']->render( 'backend_content_edit_association.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'associationClass' => $associationClass,
            'grid_menu1' => $addMenu,
            'grid_menu2' => $remMenu,
            'property' => 'user',
            'parentId' => ($user != null) ? $user->getId() : null
        ) );
    }
    
    /**
     * Firewall assigment action.
     */
    public function firewallAssigmentAction() {
        $className1 = '\Triangl\Entity\Security\Role';
        $className2 = '\Triangl\Entity\Security\Firewall';
        $associationClass = '\Triangl\Entity\Security\RoleFirewallAssociation';
        
        // Get role id.
        $role = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $roles = $repository->findAll();
        if ( count($roles) > 0 ) {
            $role = $roles[0];
        }
        
        // Menu for adding items.
        $addMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Add', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-add');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'role');
        $item->pushArg('targetProperty', 'firewall');
        $item->pushArg('method', 'add');
        $menu->pushChild($item);
        $addMenu->pushChild($menu);
        
        // Menu for removing items.
        $remMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Remove', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-remove');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'role');
        $item->pushArg('targetProperty', 'firewall');
        $item->pushArg('method', 'remove');        
        $menu->pushChild($item);
        $remMenu->pushChild($menu);
        
        $this->app['navigation.backend']->selectByRoute('backend_firewall_assigment');
        return $this->app['twig']->render( 'backend_content_edit_association.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'associationClass' => $associationClass,
            'grid_menu1' => $addMenu,
            'grid_menu2' => $remMenu,
            'property' => 'role',
            'parentId' => ($role != null) ? $role->getId() : null
        ) );
    }
    
    /**
     *  Languages action.
     */
    public function languagesAction() {
        $className = '\Triangl\Entity\ContentManagementSystem\Language';
        
        $this->app['navigation.backend']->selectByRoute('backend_languages');
        return $this->app['twig']->render( 'backend_content_single_grid_edit.html.twig', array(
            'className' => $className,
            'grid_menu' => $this->app['db.orm.grid.menu']->createMenu($className)
        ) );
    }
}
