<?php

namespace Triangl;

/**
 * Application with backend module.
 */
class BackendApplication extends BootstrapApplication {
    /**
     * Overriden.
     */
    protected function init() {
        parent::init();
        
        $this->register( new Backend() );
    }
}
