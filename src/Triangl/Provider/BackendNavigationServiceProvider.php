<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\BuildBackendMenuEvent;

/**
 * Provides functionality to build backend menu.
 */
class BackendNavigationServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {
        $app['navigation.backend'] = $app->share(function ($app) {
            $result = new MenuBuilder();
            
            // Handle which properties to display.
            $event = new BuildBackendMenuEvent($result, $app);
            $app['dispatcher']->dispatch('backend.build.navigation', $event);
            
            return $result;
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {
    }
}
