<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\BackendApiController;

/**
 * Backend api provider.
 */
class BackendApiServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        // Controllers.
        $app['backend.api.controller'] = $app->share(function() use ($app) {
            return new BackendApiController($app);
        });
        
        // Routes.
        $app->post('order/{className}/{method}/{id}/{targetId}', 'backend.api.controller:orderAction')
            ->assert('id', '\d+')
            ->value('id', null)
            ->assert('targetId', '\d+')
            ->value('targetId', null)
            ->bind('change_order');
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
