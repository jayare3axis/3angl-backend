<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\EntityGridMenu;

/**
 * Provides functionality to build menu for entity grid.
 */
class EntityGridMenuServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['db.orm.grid.menu'] = $app->share(function ($app) {
            return new EntityGridMenu($app);
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
