<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\FileManager;

/**
 * Provides functionality to build backend menu.
 */
class FileManagerServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['backend.upload.fm'] = $app->share(function ($app) {
            return new FileManager($app);
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
