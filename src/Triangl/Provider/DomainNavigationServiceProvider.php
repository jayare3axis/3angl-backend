<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;
use Triangl\BackendDomainSelector;

/**
 * Provides functionality to build domain menu.
 */
class DomainNavigationServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['navigation.domain'] = $app->share(function ($app) {
            $builder = new MenuBuilder();
                   
            $selectedDomain = $app['backend.selector.domain']->getSelectedDomain();

            $menu = new MenuItemComposite("Domains");
            foreach ( $app["security.user.instance"]->getUserDomainAssociations() as $associaton ) {
                $domain = $associaton->getDomain();
                
                $item = new MenuItemLeaf(
                    $domain->getName(), 'backend_domain'
                );            
                $item->pushArg( 'id', $domain->getId() );

                $item->setActive(false);
                if ( $domain->getId() == $selectedDomain->getId() ) {
                    $item->setActive();
                }

                $menu->pushChild($item);                
            }

            $builder->pushChild($menu);
            
            return $builder;
        });
        $app['backend.selector.domain'] = $app->share(function ($app) {
            return new BackendDomainSelector($app);
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
