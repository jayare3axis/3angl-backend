<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\Navigation\MenuBuilder;

/**
 * Provides functionality to build language menu.
 */
class LanguageNavigationServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['navigation.language'] = $app->share(function ($app) {
            return new MenuBuilder();
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
