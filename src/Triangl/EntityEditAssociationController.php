<?php

namespace Triangl;

/*
 * Used to edit entity relation.
 */
class EntityEditAssociationController extends Controller {
    /**
     * Association action.
     */
    public function associationAction($className, $filter, $property, $method, $id, $targetId) {
        $em = $this->app['db.orm.em'];        
        $repository = $em->getRepository($className);
        $targetInstance = $repository->find($targetId);
        
        $metaData = $em->getClassMetadata($className);
        $association = $metaData->getAssociationMapping($property);
        $repository = $em->getRepository($association['targetEntity']);
        $instance = $repository->find($id);
        $metaData->setFieldValue($targetInstance, $property, ($method == 'add') ? $instance : null);
        
        $em->persist($instance);
        $em->flush();
        
        $f = $this->app['db.orm.em']->getFilters()->enable($filter);
        $f->setParameter($property . '_id', $id);
        
        $grid = $this->app['db.orm.grid']->createGrid($className, -1);
        $grid->addData( 'url', $this->app->url( 'widget_grid_filter', array(
            'className' => $className,
            'filter' => $filter,
            'filterParam' => $property . '_id',
            'filterValue' => $id
        ) ) );
        
        return $this->app['twig']->render(
            'grid_default_layout.html.twig', array(
                'grid' => $grid
            )
        );
    }
    
    /**
     * Association class action.
     */
    public function associationClassAction($className, $associationClass, $property, $targetProperty, $method, $id, $targetId) {        
        $em = $this->app['db.orm.em'];
        $associationClassMetaData = $em->getClassMetadata($associationClass);
        $mapping = $associationClassMetaData->getAssociationMapping($property);
        $parentClass = $mapping["targetEntity"];
        
        // fix parent class name
        if ($parentClass[1] != "\\") {
            $parentClass = "\\" . $parentClass;
        }

        $parentInstance = $em->getRepository($parentClass)->find($id);        
        if ($method == 'add') {
            // Find inverse collection.
            $parentClassMetadata = $em->getClassMetadata($parentClass);
            $collection = null;
            foreach ($parentClassMetadata->getAssociationMappings() as $field => $mapping) {
                if ($mapping["targetEntity"] == $associationClass || "\\" . $mapping["targetEntity"] == $associationClass) {
                    $collection = $parentClassMetadata->getFieldValue($parentInstance, $field);                    
                }
            }
            $count = ($collection) ? count($collection) : 0;
            
            $targetInstance = $em->getRepository($className)->find($targetId);
            $instance = $associationClassMetaData->newInstance();
            $associationClassMetaData->setFieldValue($instance, $property, $parentInstance);
            $associationClassMetaData->setFieldValue($instance, $targetProperty, $targetInstance);
                       
            // Handle order
            if ( $associationClassMetaData->hasField('ord') ) {                
                $count = count( $em->getRepository($associationClass)->findBy( array($property => $id) ) );
                $associationClassMetaData->setFieldValue($instance, "ord", ++$count);
            }
            
            $em->persist($instance);
            $em->flush();
              
            // Fix inverse collection if necessary.
            if ($collection) {
                if ( count($collection) ==  $count) {
                    $collection->add($instance);
                }
            }
        }
        else if ($method == 'remove') {
            $instance = $em->getRepository($associationClass)->find($targetId);
            $em->remove($instance);
            $em->flush();
        }
                             
        return $this->app['widget.entity.grid.association']->indexAction($className, $parentClass, $property, $id);
    }
}
