<?php

namespace Triangl\Tests;

use Triangl\WebTestCase;
use Triangl\Backend;
use Triangl\BackendApplication;

/**
 * Functional test for Triangl Backend.
 */
class BackendTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new BackendApplication( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
    
    /**
     * Overriden.
     */
    public function testPing() {
        $this->checkRequest(Backend::DEF_URL);
        $this->checkRequest(Backend::DEF_URL . '/login');
        $this->checkRequest(Backend::DEF_URL . '/logout');
    }
}
